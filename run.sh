#!/bin/sh

if [ $# -lt 1 ]; then
  echo "usage: run.sh <inputfile>"
  exit 1;
fi

if [ ! -f $1 ]; then
  echo "file '$1' does not exist"
  exit 1;
fi

if [ ! -f "V.class" ]; then
  javac src/V.java
  mv src/V.class .
fi

java V $1
