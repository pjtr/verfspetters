import java.nio.file.*;

class VerfSpetters {
    static byte[] buf;

    public static void main(String[] args) throws Exception {
        buf = Files.readAllBytes(Paths.get(args[0]));    // Program expects one argument: path to input file.

        int i,j,min = 999, minIndex = -1;   // Initial value for min: Integer.MAX_VALUE would be better, but takes too much characters; anything larger then 144 (12x12) will do...

        byte index = 1;      // number of next splash

        for (j = 0; j < 12; j++)
            for (i = 0; i < 12; i++) {
                int size = determineSize(i, j, index);
                if (size > 0) {
                    if (size < min) {
                        min = size;
                        minIndex = index;   // Remember index number of this splash.
                    }
                    index++;
                }
            }


        String o = "";
        if (min < 999)    // If we've got a splash, go find it! All it's coordinates are marked by the value of minIndex.
            for (j = 0; j < 12; j++)
                for (i = 0; i < 12; i++)
                    if (buf[j*25+2*i] == minIndex)
                        o += ", (" + i + ", " + j + ")";
        o += "  ";
        System.out.println((minIndex>0?min:0) + " => " + o.substring(2));     // Chop off leading ", " (if there is a splash, chop off two spaces otherwise)
    }

    static int determineSize(int i, int j, byte index) {
        if (i > 11 || j > 11 || i < 0 || j < 0 || buf[j*25+2*i] <= 'O') return 0;
        // If it gets here, value == 'X', because index will never be greater than 36, the maximum number of splashes in a 12x12 square
        buf[j*25+2*i] = index;     // Mark this coordinate with the index number of the splash. Marking also avoids counting it again in any of the recursive calls.
        return determineSize(i-1,j-1, index) + determineSize(i,j-1, index) + determineSize(i+1,j-1, index)
                + determineSize(i-1,j, index) + 1 + determineSize(i+1,j, index)
                + determineSize(i-1,j+1, index) + determineSize(i,j+1, index) + determineSize(i+1,j+1, index);
    }
}
